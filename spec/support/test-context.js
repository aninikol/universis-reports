const Jasmine = require('jasmine');
// noinspection JSCheckFunctionSignatures
const jasmine = new Jasmine();
jasmine.loadConfigFile('spec/support/jasmine.json');
jasmine.execute();
