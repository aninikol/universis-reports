import {ApplicationService} from "@themost/common"; 

export declare interface ConfigurationPrintSettings {
    server: string;
    user?: string;
    password?: string;
    rootDirectory?: string;
}

export declare interface ServerResource {
    label?: string;
    uri: string;
    creationDate: any;
    updateDate?: any;
    version: string;
    resourceType: string;
}

export declare interface ReportUriReference {
    uri: string
}

export declare interface ReportDataSource {
    dataSourceReference: ReportUriReference
}

export declare interface ReportFileReference {
    jrxmlFileReference: ReportUriReference
}

export declare interface ReportInputControlReference {
    inputControlReference: ReportUriReference
}

export declare interface ServerReport {
    label?: string;
    uri: string;
    creationDate: any;
    updateDate?: any;
    version: string;
    dataSource?: ReportDataSource;
    jrxml?: ReportFileReference;
    inputControls: Array<ReportInputControlReference>;
    alwaysPromptControls?: boolean;
    controlsLayout?: string;
}

export declare interface ServerResourceLookup {
    resourceLookup: Array<ServerResource>
}

export declare class PrintService extends ApplicationService {
    /**
     * Reads a report unit and returns report definition
     * @param {any} location
     * @returns {Promise<ServerReport>}
     */
    readReport(location): Promise<ServerReport>;
    /**
     * Returns an array of items which represents the available report units
     * @param {string} location
     * @returns {Promise<Array<ServerResource>>}
     */
    readReports(location): Promise<ServerResource>;
    /**
     * Prints the given reports and returns a buffer
     * @param {string} report - A string which represents the name of the report e.g. StudentCertificate
     * @param {string} format - A string which represents the desired report format e.g. pdf, html etc
     * @param {*=} queryParams
     * @param {*=} extraHeaders 
     * @returns {Promise<any>}
     */
    print(report: string, format: string, queryParams?: any, extraHeaders?: any): Promise<Buffer>;
}
